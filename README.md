# README #

### Tax Calculator Coding Exercise Solution ###

### Assumptions ###
* base tax for bracket calculated based on tackets beneath it
* Payments only occur for a given month of a year, not over 2 months or across a month
* Thus, payments only start on the first of a month and are monthly
* input and output format independently defined

### How do I get set up? ###
* Right now, this setup process is fairly untested.
* Maven lifecycle scripts can be used to pull dependencies run tests and deploy a .JAR file, which can then be ran.
* I have mostly tested this through maven integrations in IntelliJ.
* Two JAR files will be created, one with and one without dependencies included.

### Who do I talk to? ###
alexdiakovsky@gmail.com / @alexdia25

### Todo / Known Defects ###
* pull super calculation out into its own thing
* don't hardcode tax year threshold
* consider pulling tax year out of data, specify on runtime for employees
* reduce main (domain modeling issue?)
* input shouldn't be able to not have fields needed. Need to make this fail.
* output code need to be rewritten to be more testable (would be way better to actually check content of file too) 
    * mocking?
    * output stream?
* memoize base tax generation
* memoize tax calculators
* put things in main elsewhere (if it makes sense)
* cleanup output files, make more generic picking writer
* implement CSV employee input parsing
* make payment period more flexible (weekly? fortnightly?)

### Peer Feedback ###
* MARKO - Interface for InputFilesStruct?
* MARKO - TaxCalculator - for readability sake, maybe extract some variables
* MARKO - Use a Factory Interface in payment?
* MARKO - decorator pattern (employee, payment)
* MARKO - logic inside constructor - google it - is it bad
* MARKO - get tax year no hard coding - extract - 7
* MARKO - get tax year - domains - where should what go - taxyear and employee?
* JASPER - way too much in main
* JASPER - domain modelling
* JASPER - potentially overthinking/overcomplicating tax year switching - could try just reading from config to make 
    it simpler and cleaner