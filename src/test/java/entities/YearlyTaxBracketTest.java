package entities;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

public class YearlyTaxBracketTest {

    TaxBracket lowestTaxBracket;
    TaxBracket midTaxBracket;
    TaxBracket highestTaxBracket;
    List<TaxBracket> taxBrackets;
    YearlyTaxBracket yearlyTaxBracket;

    @Before
    public void setUp() {
        lowestTaxBracket = new TaxBracket(0D,10000D, 0D);
        midTaxBracket = new TaxBracket (10001D, 20000D, 10D);
        highestTaxBracket = new TaxBracket (20001D, 20D);
        taxBrackets = new ArrayList<TaxBracket>();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void YearlyTaxBracket_InstanceCreated_WhenGivenValidParams(){
        taxBrackets.add(lowestTaxBracket);
        taxBrackets.add(midTaxBracket);
        taxBrackets.add(highestTaxBracket);
        yearlyTaxBracket = new YearlyTaxBracket(2013, taxBrackets);
        Assert.assertTrue(yearlyTaxBracket.getTaxBrackets() == taxBrackets);
    }

}
