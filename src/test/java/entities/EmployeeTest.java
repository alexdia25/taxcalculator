package entities;

import exceptions.FatalException;
import javafx.util.Pair;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class EmployeeTest {

    @Before
    public void setUp() {
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void EmployeeInstance_Created_WhenGivenValidParams(){
        Employee employee = new Employee("David", "Rudd", "60050", "9%", "01 March 2013 – 31 March 2013" );
        assert(employee.getFirstName() == "David");
        assert(employee.getLastName() == "Rudd");
        assert(employee.getAnnualSalary() == 60050.0);
        assert(employee.getSuperRatePercent() == 9.0);
        assert(employee.getPaymentStartDate() == "01 March 2013 – 31 March 2013");
    }

    @Test
    public void EmployeeInstance_ThrowsException_WhenInvalidSalary(){
        thrown.expect(NumberFormatException.class);
        Employee employee = new Employee("David", "Rudd", "hello", "9%", "01 March 2013 – 31 March 2013" );
    }

    @Test
    public void EmployeeInstance_ThrowsException_WhenInvalidSuperRate(){
        thrown.expect(NumberFormatException.class);
        Employee employee = new Employee("David", "Rudd", "60050", "A", "01 March 2013 – 31 March 2013" );
    }

    @Test
    public void EmployeeInstance_ConvertsSuperRateToDouble_WhenGivenSuperRateWithPercentSign(){
        Employee employee = new Employee("David", "Rudd", "60050", "9%", "01 March 2013 – 31 March 2013" );
        assert(employee.getSuperRatePercent() == 9.0);
    }

    @Test
    public void EmployeeInstance_ConvertsSuperRateToDouble_WhenGivenSuperRateWithNoPercentSign(){
        Employee employee = new Employee("David", "Rudd", "60050", "9", "01 March 2013 – 31 March 2013" );
        assert(employee.getSuperRatePercent() == 9.0);
    }

    @Test
    public void GetDateRange_ReturnsCorrectDateRange_WhenGivenValidDateFields(){
        Employee employee = new Employee("David", "Rudd", "60050", "9%", "01 March 2013 – 31 March 2013" );
        Pair<DateTime,DateTime> dateRange = new Pair<>(new DateTime(), new DateTime());
        try{
            dateRange = employee.getPaymentDateRange();
        }catch( FatalException p1){
            System.err.println(p1.getMessage());
        }
        assert(dateRange.getKey().getDayOfMonth()==01);
        assert(dateRange.getKey().getMonthOfYear()==03);
        assert(dateRange.getKey().getYear()==2013);
        assert(dateRange.getValue().getDayOfMonth()==31);
        assert(dateRange.getValue().getMonthOfYear()==03);
        assert(dateRange.getValue().getYear()==2013);
    }

    @Test
    public void GetDateRange_ThrowsException_WhenGivenInValidDateFields(){
        Employee employee = new Employee("David", "Rudd", "60050", "9%", "01-03-2013 - 31-03-2013" );
        Pair<DateTime,DateTime> dateRange = new Pair<DateTime,DateTime>(new DateTime(0,12,15,0,0), new DateTime(0,12,15,0,0));
        try{
            dateRange = employee.getPaymentDateRange();
        }catch( FatalException p1){
            System.err.println(p1.getMessage());
        }
        assert(dateRange.getKey().getDayOfMonth()!=01);
        assert(dateRange.getKey().getMonthOfYear()!=03);
        assert(dateRange.getKey().getYear()!=2013);
        assert(dateRange.getValue().getDayOfMonth()!=31);
        assert(dateRange.getValue().getMonthOfYear()!=03);
        assert(dateRange.getValue().getYear()!=2013);
    }
}
