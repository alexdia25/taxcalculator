package entities;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import services.PaymentCalculators.TaxCalculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class EmployeePaymentTest {

    Employee employee;
    Employee employee2;
    TaxBracket firstTaxBracket;
    TaxBracket secondTaxBracket;
    TaxBracket thirdTaxBracket;
    TaxBracket fourthTaxBracket;
    List<TaxBracket> taxBrackets;
    TaxCalculator taxCalculator;
    EmployeePayment employeePayment;
    EmployeePayment employee2Payment;

    @Before
    public void setUp() {
        employee = new Employee("David", "Rudd", "60050", "9%", "01 March 2013 – 31 March 2013");
        employee2 = new Employee("Ryan", "Chen", "120000", "10%", "01 March 2013 – 31 March 2013");
        firstTaxBracket = new TaxBracket(0D,18200D,0D);
        secondTaxBracket = new TaxBracket (18201D, 37000D,19D);
        thirdTaxBracket = new TaxBracket (37001D, 80000D, 32.5);
        fourthTaxBracket = new TaxBracket (80001D, 180000D, 37D);
        taxBrackets = new ArrayList<TaxBracket>();
        taxBrackets.add(firstTaxBracket);
        taxBrackets.add(secondTaxBracket);
        taxBrackets.add(thirdTaxBracket);
        taxBrackets.add(fourthTaxBracket);
        taxCalculator = new TaxCalculator(taxBrackets);
        employeePayment = new EmployeePayment(employee,taxCalculator);
        employee2Payment = new EmployeePayment(employee2,taxCalculator);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void EmployeePayment_GeneratesCorrectFullName_WhenInitialized() {
        assert(Objects.equals(employeePayment.getEmployeeFullName(), "David Rudd"));
        assert(Objects.equals(employee2Payment.getEmployeeFullName(), "Ryan Chen"));
    }

    @Test
    public void EmployeePayment_CalculatesCorrectRoundedGrossIncome_WhenInitialized() {
        assert(employeePayment.getRoundedGrossIncome() == 5004L);
        assert(employee2Payment.getRoundedGrossIncome() == 10000L);
    }

    @Test
    public void EmployeePayment_CalculatesCorrectRoundedIncomeTax_WhenInitialized() {
        assert(employeePayment.getRoundedIncomeTax() == 922L);
        assert(employee2Payment.getRoundedIncomeTax() == 2696L);
    }

    @Test
    public void EmployeePayment_CalculatesCorrectNetIncome_WhenInitialized() {
        assert(employeePayment.getNetIncome() == 4082L);
        assert(employee2Payment.getNetIncome() == 7304L);
    }

    @Test
    public void EmployeePayment_CalculatesCorrectSuperPayment_WhenInitialized() {
        assert(employeePayment.getRoundedSuperannuation() == 450L);
        assert(employee2Payment.getRoundedSuperannuation() == 1000L);
    }

}
