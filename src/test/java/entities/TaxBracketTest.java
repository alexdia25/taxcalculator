package entities;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TaxBracketTest {

    TaxBracket taxBracket;

    @Before
    public void setUp() {

    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void TaxBracket_Initializes_With3Arguments() {
        taxBracket = new TaxBracket (18201D, 37000D,19D);
        assert(taxBracket.getUpperSalaryThreshold() == 37000D);
    }

    @Test
    public void TaxBracket_Initializes_With2Arguments() {
        taxBracket = new TaxBracket (18201D, 19D);
        assert(taxBracket.getUpperSalaryThreshold() == Double.MAX_VALUE);
    }

    @Test
    public void TaxBracketTaxCents_ConvertsToDecimal_WhenGivenRawCents() {
        taxBracket = new TaxBracket (18201D, 37000D,19D);
        assert(taxBracket.getTaxOnDollar() == .19D);
    }


}
