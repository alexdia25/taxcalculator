package services.PaymentCalculators;

import entities.TaxBracket;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

public class TaxCalculatorTest {

    TaxBracket lowestTaxBracket;
    TaxBracket midTaxBracket;
    TaxBracket highestTaxBracket;
    List<TaxBracket> taxBrackets;
    TaxCalculator taxCalculator;

    @Before
    public void setUp() {
        lowestTaxBracket = new TaxBracket(0D,10000D,0D);
        midTaxBracket = new TaxBracket (10001D, 20000D,10D);
        highestTaxBracket = new TaxBracket (20001D, Double.MAX_VALUE, 20D);
        taxBrackets = new ArrayList<TaxBracket>();
        taxBrackets.add(lowestTaxBracket);
        taxBrackets.add(midTaxBracket);
        taxBrackets.add(highestTaxBracket);
        taxCalculator = new TaxCalculator(taxBrackets);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void taxCalculator_returnsCorrectAnnualTax_WhenGivenInRangeBracketSalary(){
        Assert.assertTrue(0D == taxCalculator.calculateAnnualSalaryTax(5000D));
    }

    @Test
    public void taxCalculator_returnsCorrectAnnualTax_WhenGivenLowerBoundBracketSalary(){
        Assert.assertTrue(0D == taxCalculator.calculateAnnualSalaryTax(0D));
        Assert.assertTrue(0.1D == taxCalculator.calculateAnnualSalaryTax(10001D));
        Assert.assertTrue(1000.2D == taxCalculator.calculateAnnualSalaryTax(20001D));
    }

    @Test
    public void taxCalculator_returnsCorrectAnnualTax_WhenGivenUpperBoundBracketSalary(){
        Assert.assertTrue(0D == taxCalculator.calculateAnnualSalaryTax(10000D));
        Assert.assertTrue(1000D == taxCalculator.calculateAnnualSalaryTax(20000D));
    }

    @Test
    public void taxCalculator_returnsCorrectAnnualTax_WhenGivenAboveAllBracketSalary(){
        Assert.assertTrue(3000D == taxCalculator.calculateAnnualSalaryTax(30000D));
    }

    @Test
    public void taxCalculator_returnsCorrectAnnualTax_WhenGivenNegativeSalary(){
        Assert.assertTrue(0D == taxCalculator.calculateAnnualSalaryTax(-100D));
    }

    @Test
    public void taxCalculator_returnsCorrectMonthlyTax_WhenGivenValidBracketSalary(){
        Assert.assertTrue(0D == taxCalculator.calculateMonthlySalaryTax(5000D));
        Assert.assertTrue((0.1D/12) == taxCalculator.calculateMonthlySalaryTax(10001D));
        Assert.assertTrue((1000.2D/12) == taxCalculator.calculateMonthlySalaryTax(20001D));
        Assert.assertTrue(250D == taxCalculator.calculateMonthlySalaryTax(30000D));
    }
}
