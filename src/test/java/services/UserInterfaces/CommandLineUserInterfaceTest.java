package services.UserInterfaces;

import exceptions.FatalException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class CommandLineUserInterfaceTest {
    @Before
    public void setUp() {
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void commandLineHandler_Succeeds_WhenGivenValidParams() throws FatalException {
        String[] args = new String[] {"JSON", "./employees.json", "JSON", "./taxbrackets.json", "./outputlocation.json"} ;
        InputFilesStruct inputFilesStruct = CommandLineUserInterface.retrieveInputsFromArguments(args);
        assertEquals("JSON", inputFilesStruct.getEmployeeInputStreamType());
        assertEquals("JSON", inputFilesStruct.getTaxbracketInputType());
        assertEquals("./outputlocation.json", inputFilesStruct.getOutputLocation());
    }

    @Test
    public void commandLineHandler_ThrowsException_WhenGivenIncorrectNumberOfParams() throws FatalException {
        thrown.expect(FatalException.class);
        thrown.expectMessage("Tax Calculator takes exactly five arguments.");
        String[] args = new String[] {"Hello"};
        InputFilesStruct inputFilesStruct = CommandLineUserInterface.retrieveInputsFromArguments(args);
    }

    @Test
    public void commandLineHandler_ThrowsException_WhenGivenInvalidEmployeeInputTypeParam() throws FatalException {
        thrown.expect(FatalException.class);
        thrown.expectMessage("Unable to read contents of ./inputtest.csv input file.");
        String[] args = new String[] {"JSON", "./inputtest.csv", "JSON", "./taxbrackets.csv", "./outputlocation.json"};
        InputFilesStruct inputFilesStruct = CommandLineUserInterface.retrieveInputsFromArguments(args);
    }

    @Test
    public void commandLineHandler_ThrowsException_WhenGivenInvalidTaxBracketInputTypeParam() throws FatalException {
        thrown.expect(FatalException.class);
        thrown.expectMessage("Unable to read contents of ./inputtest.json input file.");
        String[] args = new String[] {"JSON", "./employees.json", "JSON", "./inputtest.json", "./outputlocation.json"};
        InputFilesStruct inputFilesStruct = CommandLineUserInterface.retrieveInputsFromArguments(args);
    }
}
