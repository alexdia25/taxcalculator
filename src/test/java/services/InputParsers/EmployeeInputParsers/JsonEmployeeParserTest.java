package services.InputParsers.EmployeeInputParsers;

import entities.Employee;
import exceptions.FatalException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Objects;

public class JsonEmployeeParserTest {

    JsonEmployeeParser parser;

    @Before
    public void setUp() {
        parser = new JsonEmployeeParser();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void JsonEmployeeParser_Succeeds_WhenValidJsonProvided() throws FatalException {
        String jsonString =
                "[\n" +
                        "  {\n" +
                        "    \"firstName\": \"David\",\n" +
                        "    \"lastName\": \"Rudd\",\n" +
                        "    \"annualSalary\": \"60050\",\n" +
                        "    \"superRate\": \"9%\",\n" +
                        "    \"paymentStartDate\": \"01 March 2013 – 31 March 2013\"\n" +
                        "  }\n" +
                        "]";
        InputStream input = new ByteArrayInputStream(jsonString.getBytes());
        List<Employee> employees = parser.parseEmployees(input);
        assert(employees.size()==1);
        assert(Objects.equals(employees.get(0).getLastName(), "Rudd"));
    }

    @Test
    public void JsonEmployeeParser_Fails_WhenInvalidJsonFieldsProvided() throws FatalException {
        thrown.expect(FatalException.class);
        thrown.expectMessage("Unable to read in employee input file. Check data format.");
        thrown.expectMessage("Unrecognized field \"annualCelery\"");
        String badJsonFieldString =
                "[\n" +
                        "  {\n" +
                        "    \"firstName\": \"David\",\n" +
                        "    \"lastName\": \"Rudd\",\n" +
                        "    \"annualCelery\": \"60050\",\n" +
                        "    \"superRate\": \"9%\",\n" +
                        "    \"paymentStartDate\": \"01 March 2013 – 31 March 2013\"\n" +
                        "  }\n" +
                        "]";
        InputStream input = new ByteArrayInputStream(badJsonFieldString.getBytes());
        List<Employee> employees = parser.parseEmployees(input);
        assert(employees.size()==0);
    }

    @Test
    public void JsonEmployeeParser_Fails_WhenInvalidDataProvided() throws FatalException {
        thrown.expect(FatalException.class);
        thrown.expectMessage("Unable to read in employee input file. Check data format.");
        thrown.expectMessage("Cannot deserialize value of type `java.lang.Double` from String \"Celery\": not a valid Double value");
        String badJsonDataString =
                "[\n" +
                        "  {\n" +
                        "    \"firstName\": \"David\",\n" +
                        "    \"lastName\": \"Rudd\",\n" +
                        "    \"annualSalary\": \"Celery\",\n" +
                        "    \"superRate\": \"9%\",\n" +
                        "    \"paymentStartDate\": \"01 March 2013 – 31 March 2013\"\n" +
                        "  }\n" +
                        "]";
        InputStream input = new ByteArrayInputStream(badJsonDataString.getBytes());
        List<Employee> employees = parser.parseEmployees(input);
        assert(employees.size()==0);
    }

    @Test
    public void JsonEmployeeParser_Passes_WhenJsonMissingFieldProvided() throws FatalException {
        String badJsonFieldMissingString =
                "[\n" +
                        "  {\n" +
                        "    \"firstName\": \"David\",\n" +
                        "    \"lastName\": \"Rudd\",\n" +
                        "    \"superRate\": \"9%\",\n" +
                        "    \"paymentStartDate\": \"01 March 2013 – 31 March 2013\"\n" +
                        "  }\n" +
                        "]";
        InputStream input = new ByteArrayInputStream(badJsonFieldMissingString.getBytes());
        List<Employee> employees = parser.parseEmployees(input);
        assert(employees.size()==1);
    }
}
