package services.InputParsers.EmployeeInputParsers;

import exceptions.FatalException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class EmployeeParserFactoryTest {

    @Before
    public void setUp() {
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void EmployeeParserFactory_Succeeds_WhenJSONFileType() throws FatalException {
        EmployeeParser parser = EmployeeParserFactory.getEmployeeParser("JSON");
        assert(parser.getClass() == JsonEmployeeParser.class);
    }

    @Test
    public void EmployeeParserFactory_ThrowsException_WhenInvalidFileType() throws FatalException {
        thrown.expect(FatalException.class);
        thrown.expectMessage("EmployeeParser must be called with valid input file type.");
        EmployeeParserFactory.getEmployeeParser("hello");
    }
}
