package services.InputParsers.TaxBracketInputParsers;

import entities.TaxBracket;

import exceptions.FatalException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

public class JsonTaxBracketParserTest {

    JsonTaxBracketParser parser;

    @Before
    public void setUp() {
        parser = new JsonTaxBracketParser();
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void JsonTaxBracketParser_Succeeds_WhenValidJsonProvided() throws FatalException {
        String jsonString =
                "[\n" +
                "  {\n" +
                "    \"startTaxYear\": 2012,\n" +
                "    \"taxBrackets\": [\n" +
                "      {\n" +
                "        \"upperSalaryThreshold\": 0,\n" +
                "        \"lowerSalaryThreshold\": 0,\n" +
                "        \"taxOnDollar\": 50\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "]";
        InputStream input = new ByteArrayInputStream(jsonString.getBytes());

        HashMap<Integer, List<TaxBracket>> yearlyTaxBrackets = parser.parseTaxBrackets(input);
        Assert.assertTrue(yearlyTaxBrackets.size() == 1);
        Assert.assertTrue(yearlyTaxBrackets.get(2012).get(0).getTaxOnDollar() == .5D);
    }

    @Test
    public void JsonTaxBracketParser_Fails_WhenInvalidJsonFieldsProvided() throws FatalException {
        thrown.expect(FatalException.class);
        thrown.expectMessage("Unable to read in tax bracket input file. Check data format.");
        thrown.expectMessage("Unrecognized field \"upperCeleryThreshold\"");
        String badJsonFieldsString =
                "[\n" +
                        "  {\n" +
                        "    \"startTaxYear\": 2012,\n" +
                        "    \"taxBrackets\": [\n" +
                        "      {\n" +
                        "        \"upperCeleryThreshold\": 0,\n" +
                        "        \"lowerCeleryThreshold\": 0,\n" +
                        "        \"taxOnDollar\": 50\n" +
                        "      }\n" +
                        "    ]\n" +
                        "  }\n" +
                        "]";
        InputStream input = new ByteArrayInputStream(badJsonFieldsString.getBytes());
        HashMap<Integer, List<TaxBracket>> taxBrackets = parser.parseTaxBrackets(input);
        assert(taxBrackets.size()==0);
    }

    @Test
    public void JsonTaxBracketParser_Fails_WhenInvalidJsonDataProvided() throws FatalException {
        thrown.expect(FatalException.class);
        thrown.expectMessage("Unable to read in tax bracket input file. Check data format.");
        thrown.expectMessage("Cannot deserialize value of type `java.lang.Double` from String \"Celery\": not a valid Double value");
        String badJsonDataString =
                "[\n" +
                        "  {\n" +
                        "    \"startTaxYear\": 2012,\n" +
                        "    \"taxBrackets\": [\n" +
                        "      {\n" +
                        "        \"upperSalaryThreshold\": \"Celery\",\n" +
                        "        \"lowerSalaryThreshold\": 0,\n" +
                        "        \"taxOnDollar\": 50\n" +
                        "      }\n" +
                        "    ]\n" +
                        "  }\n" +
                        "]";
        InputStream input = new ByteArrayInputStream(badJsonDataString.getBytes());
        HashMap<Integer, List<TaxBracket>> taxBrackets = parser.parseTaxBrackets(input);
        assert(taxBrackets.size()==0);
    }

    @Test
    public void JsonTaxBracketParser_Passes_WhenInvalidJsonMissingFieldProvided() throws FatalException {
        String badJsonMissingFieldString =
                "[\n" +
                        "  {\n" +
                        "    \"startTaxYear\": 2012,\n" +
                        "    \"taxBrackets\": [\n" +
                        "      {\n" +
                        "        \"lowerSalaryThreshold\": 0,\n" +
                        "        \"taxOnDollar\": 50\n" +
                        "      }\n" +
                        "    ]\n" +
                        "  }\n" +
                        "]";
        InputStream input = new ByteArrayInputStream(badJsonMissingFieldString.getBytes());
        HashMap<Integer, List<TaxBracket>> taxBrackets = parser.parseTaxBrackets(input);
        assert(taxBrackets.size()==1);
    }
}
