package services.InputParsers.TaxBracketInputParsers;

import exceptions.FatalException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class TaxBracketParserFactoryTest {

    @Before
    public void setUp() {
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void TaxBracketParserFactory_Succeeds_WhenValidFileType() throws FatalException {
        TaxBracketParser parser = TaxBracketParserFactory.getTaxBracketParser("JSON");
        assert(parser.getClass() == JsonTaxBracketParser.class);
    }

    @Test
    public void TaxBracketParserFactory_ThrowsException_WhenInvalidFileType() throws FatalException {
        thrown.expect(FatalException.class);
        thrown.expectMessage("TaxBracketParserFactory must be called with valid input file type.");
        TaxBracketParserFactory.getTaxBracketParser("hello");
    }
}
