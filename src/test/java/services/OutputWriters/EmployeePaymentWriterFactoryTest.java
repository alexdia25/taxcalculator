package services.OutputWriters;

import exceptions.FatalException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class EmployeePaymentWriterFactoryTest {

    @Before
    public void setUp() {
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void EmployeePaymentWriterFactory_Succeeds_WhenJSONFileType() throws FatalException {
        EmployeePaymentWriter parser = EmployeePaymentWriterFactory.getEmployeePaymentWriter("JSON");
        assert(parser.getClass() == JsonEmployeePaymentWriter.class);
    }

    @Test
    public void EmployeePaymentWriterFactory_ThrowsException_WhenInvalidFileType() throws FatalException {
        thrown.expect(FatalException.class);
        thrown.expectMessage("No EmployeePaymentWriter exists for " + "hello" + " output type.");
        EmployeePaymentWriterFactory.getEmployeePaymentWriter("hello");
    }
}
