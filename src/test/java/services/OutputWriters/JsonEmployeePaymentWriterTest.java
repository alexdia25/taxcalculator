package services.OutputWriters;

import entities.Employee;
import entities.EmployeePayment;
import entities.TaxBracket;
import exceptions.FatalException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import services.PaymentCalculators.TaxCalculator;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class JsonEmployeePaymentWriterTest {

    JsonEmployeePaymentWriter parser;
    Employee employee;
    TaxBracket firstTaxBracket;
    List<TaxBracket> taxBrackets;
    TaxCalculator taxCalculator;
    EmployeePayment employeePayment;
    List<EmployeePayment> employeePaymentList;

    @Before
    public void setUp() {
        parser = new JsonEmployeePaymentWriter();
        employee = new Employee("David", "Rudd", "60050", "9%", "01 March 2013 – 31 March 2013");
        firstTaxBracket = new TaxBracket(0D,18200D,0D);
        taxBrackets = new ArrayList<TaxBracket>();
        taxBrackets.add(firstTaxBracket);
        taxCalculator = new TaxCalculator(taxBrackets);
        employeePayment = new EmployeePayment(employee,taxCalculator);
        employeePaymentList = new ArrayList<>();
        employeePaymentList.add(employeePayment);
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void JsonEmployeeParser_Succeeds_WhenValidJsonProvided() throws FatalException {
        EmployeePaymentWriter writer = EmployeePaymentWriterFactory.getEmployeePaymentWriter("JSON");
        String outputFileName = "./testpayments.json";
        writer.writePayments(employeePaymentList, outputFileName);
        File outputPaymentsFile = new File(outputFileName);
        assert(outputPaymentsFile.exists());
    }
}
