package entities;

import services.PaymentCalculators.TaxCalculator;

public class EmployeePayment {

    private String employeeFullName;
    private String payPeriod;
    private Double grossIncome;
    private Double incomeTax;
    private Long netIncome;
    private Double superannuation;

    public EmployeePayment(Employee employee, TaxCalculator taxCalculator) {
        this.employeeFullName = employee.getFirstName() + " " + employee.getLastName();
        this.payPeriod = employee.getPaymentStartDate();
        this.grossIncome = employee.getAnnualSalary()/12;
        this.superannuation = this.grossIncome*(employee.getSuperRatePercent()/100);
        setIncomeTaxAndNetIncome(taxCalculator.calculateMonthlySalaryTax(employee.getAnnualSalary()));
    }

    public String getEmployeeFullName() {
        return employeeFullName;
    }

    public void setEmployeeFullName(String employeeFullName) {
        this.employeeFullName = employeeFullName;
    }

    public String getPayPeriod() {
        return payPeriod;
    }

    public void setPayPeriod(String payPeriod) {
        this.payPeriod = payPeriod;
    }

    public Double getGrossIncome() {
        return grossIncome;
    }

    public Long getRoundedGrossIncome() { return Math.round(grossIncome); }

    public void setGrossIncome(Double grossIncome) {
        this.grossIncome = grossIncome;
    }

    public Double getIncomeTax() {
        return incomeTax;
    }

    public Long getRoundedIncomeTax() { return Math.round(incomeTax); }

    public void setIncomeTax(Double incomeTax) {
        this.incomeTax = incomeTax;
    }

    public void setIncomeTaxAndNetIncome(Double incomeTax) {
        this.incomeTax = incomeTax;
        this.netIncome = this.getRoundedGrossIncome() - this.getRoundedIncomeTax();
    }

    public Long getNetIncome() {
        return netIncome;
    }

    public void setNetIncome(Long netIncome) {
        this.netIncome = netIncome;
    }

    public Double getSuperannuation() {
        return superannuation;
    }

    public void setSuperannuation(Double superannuation) {
        this.superannuation = superannuation;
    }

    public Long getRoundedSuperannuation() { return Math.round(superannuation); }

}
