package entities;

import java.util.List;

public class YearlyTaxBracket {

    private Integer startTaxYear;
    private List<TaxBracket> taxBrackets;

    public Integer getStartTaxYear() {
        return startTaxYear;
    }

    public void setStartTaxYear(Integer startTaxYear) {
        this.startTaxYear = startTaxYear;
    }

    public List<TaxBracket> getTaxBrackets() {
        return taxBrackets;
    }

    public void setTaxBrackets(List<TaxBracket> taxBrackets) {
        this.taxBrackets = taxBrackets;
    }

    public YearlyTaxBracket(Integer startTaxYear, List<TaxBracket> taxBrackets) {
        this.startTaxYear = startTaxYear;
        this.taxBrackets = taxBrackets;
    }

    public YearlyTaxBracket(){};
}


