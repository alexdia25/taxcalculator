package entities;

public class TaxBracket {

    private Double lowerSalaryThreshold;
    private Double upperSalaryThreshold = Double.MAX_VALUE;
    private Double taxOnDollar;

    public Double getLowerSalaryThreshold() {
        return lowerSalaryThreshold;
    }

    public void setLowerSalaryThreshold(Double lowerSalaryThreshold) {
        this.lowerSalaryThreshold = lowerSalaryThreshold;
    }

    public Double getUpperSalaryThreshold() {
        return upperSalaryThreshold;
    }

    public void setUpperSalaryThreshold(Double upperSalaryThreshold) {
        this.upperSalaryThreshold = upperSalaryThreshold;
    }

    public Double getTaxOnDollar() {
        return taxOnDollar;
    }

    public void setTaxOnDollar(Double taxCentsOnDollar) {
        this.taxOnDollar = taxCentsOnDollar/100;
    }

    public TaxBracket(Double lowerSalaryThreshold, Double upperSalaryThreshold, Double taxCentsOnDollar) {
        setLowerSalaryThreshold(lowerSalaryThreshold);
        setUpperSalaryThreshold(upperSalaryThreshold);
        setTaxOnDollar(taxCentsOnDollar);
    }

    public TaxBracket(Double lowerSalaryThreshold, Double taxCentsOnDollar) {
        setLowerSalaryThreshold(lowerSalaryThreshold);
        setUpperSalaryThreshold(Double.MAX_VALUE);
        setTaxOnDollar(taxCentsOnDollar);
    }

    public TaxBracket(){};
}