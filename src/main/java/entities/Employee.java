package entities;

import exceptions.FatalException;
import javafx.util.Pair;
import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Employee {

    private String firstName;
    private String lastName;
    private Double annualSalary;
    private Double superRatePercent;
    private String paymentStartDate;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Double getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(Double annualSalary) {
        this.annualSalary = annualSalary;
    }

    public Double getSuperRatePercent() {
        return superRatePercent;
    }

    public void setSuperRatePercent(Double superRatePercent) {
        this.superRatePercent = superRatePercent;
    }

    public void setSuperRate(String superRate) {
        this.superRatePercent = getDoubleSuperRateFromString(superRate);
    }

    public String getPaymentStartDate() {
        return paymentStartDate;
    }

    public void setPaymentStartDate(String paymentStartDate) {
        this.paymentStartDate = paymentStartDate;
    }

    public Pair<DateTime,DateTime> getPaymentDateRange() throws FatalException {

        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd MMM yyyy");
        String[] splitDates = this.paymentStartDate.split(" .? ");

        DateTime startDate = null;
        DateTime endDate = null;
        try {
            startDate = new DateTime(dateFormatter.parse(splitDates[0]));
            endDate = new DateTime(dateFormatter.parse(splitDates[1]));
        } catch (ParseException e) {
            throw new FatalException("Unable to parse start and/or end dates. Check input date format." +
                    "Valid Example: 01 March 2013 - 31 March 2013", e);
        }

        Pair<DateTime,DateTime> returnDates = new Pair<DateTime,DateTime>(startDate,endDate);

        return returnDates;
    }

    public Employee(String firstName, String lastName, String annualSalary, String superRatePercent, String paymentStartDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.annualSalary = Double.parseDouble(annualSalary);
        this.superRatePercent = getDoubleSuperRateFromString(superRatePercent);
        this.paymentStartDate = paymentStartDate;
    }

    private Double getDoubleSuperRateFromString(String superRate) {
        return Double.parseDouble(superRate.replaceAll("%",""));
    }

    public Employee(){
    };

}

