package services.UserInterfaces;

import exceptions.FatalException;

import java.io.IOException;
import java.io.InputStream;

public class CommandLineUserInterface {
    public static InputFilesStruct retrieveInputsFromArguments(String[] args) throws FatalException{
        InputFilesStruct inputFilesStruct;
        try {
            inputFilesStruct = new InputFilesStruct(args[0], getStreamFromFile(args[1]), args[2], getStreamFromFile(args[3]), args[4]);
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new FatalException("Tax Calculator takes exactly five arguments." + getCommandLineHelp(), e);
        }
        return inputFilesStruct;
    }

    private static InputStream getStreamFromFile(String fileName) throws FatalException {
        try {
            InputStream fileStream = ClassLoader.getSystemResourceAsStream(fileName);
            if(fileStream == null){
                throw new IOException();
            }
            return fileStream;
        } catch (IOException e) {
            throw new FatalException("Unable to read contents of "+ fileName + " input file.", e);
        }
    }

    private static String getCommandLineHelp(){
        return("\nTax Calculator Usage:" +
                "\n[application] [EmployeeInputFormat] [EmployeeInputFile] [TaxBracketInputFormat] [TaxBracketInputFile] [Output Location]" +
                "\nEmployeeInputFormat - format of the input data. Currently only supports 'JSON'." +
                "\nEmployeeInputFile - file path to the input data. Example: 'employees.csv'." +
                "\nTaxBracketInputFormat - format of the input data. Currently only supports 'JSON'." +
                "\nTaxBracketInputFile - file path to the input data. Example: 'taxbrackets.json'." +
                "\nOutputLocation - file path to the desired output location");
    }


}
