package services.UserInterfaces;

import exceptions.FatalException;

import java.io.InputStream;

public class InputFilesStruct {
    String employeeInputStreamType;
    InputStream employeeInput;
    String taxbracketInputType;
    InputStream taxBracketInput;
    String outputLocation;

    public InputFilesStruct(String employeeInputStreamType, InputStream employeeInput, String taxbracketInputType,
                            InputStream taxBracketInput, String outputLocation){
        this.employeeInputStreamType = employeeInputStreamType;
        this.employeeInput = employeeInput;
        this.taxbracketInputType = taxbracketInputType;
        this.taxBracketInput = taxBracketInput;
        this.outputLocation = outputLocation;
    }

    public String getEmployeeInputStreamType() {
        return employeeInputStreamType;
    }

    public InputStream getEmployeeInput() {
        return employeeInput;
    }

    public String getTaxbracketInputType() {
        return taxbracketInputType;
    }

    public InputStream getTaxBracketInput() {
        return taxBracketInput;
    }

    public String getOutputLocation() {
        return outputLocation;
    }
}
