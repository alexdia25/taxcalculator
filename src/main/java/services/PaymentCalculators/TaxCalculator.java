package services.PaymentCalculators;

import entities.TaxBracket;

import java.util.List;

public class TaxCalculator{

    private List<TaxBracket> taxBrackets;

    public TaxCalculator(List<TaxBracket> taxBrackets){
        this.taxBrackets = taxBrackets;
    }

    public Double calculateAnnualSalaryTax(Double annualSalary){

        Double calculatedTax = 0D;

        for(TaxBracket taxBracket: this.taxBrackets){
            Double upperSalaryThreshold = taxBracket.getUpperSalaryThreshold();
            Double lowerSalaryThreshold = taxBracket.getLowerSalaryThreshold();
            Double taxCentsPerDollar = taxBracket.getTaxOnDollar();
            if(annualSalary > upperSalaryThreshold){
                calculatedTax += ((upperSalaryThreshold-lowerSalaryThreshold)+1) * taxCentsPerDollar;
            }
            if(annualSalary >= lowerSalaryThreshold && annualSalary <= upperSalaryThreshold){
                calculatedTax += (((annualSalary-lowerSalaryThreshold)+1) * taxCentsPerDollar);
            }
        }

        return calculatedTax;

    };

    public Double calculateMonthlySalaryTax(Double annualSalary){
        return (calculateAnnualSalaryTax(annualSalary)/12);
    };

}
