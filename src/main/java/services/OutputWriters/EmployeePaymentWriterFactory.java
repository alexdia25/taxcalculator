package services.OutputWriters;

import exceptions.FatalException;

public class EmployeePaymentWriterFactory {

    public static EmployeePaymentWriter getEmployeePaymentWriter(String writerType) throws FatalException{
        JsonEmployeePaymentWriter writer;
        switch(writerType.toUpperCase()){
            case "JSON":
                writer = new JsonEmployeePaymentWriter();
                break;
            default:
                throw new FatalException("No EmployeePaymentWriter exists for " + writerType + " output type.", new IllegalArgumentException());
        }
        return writer;
    }
}
