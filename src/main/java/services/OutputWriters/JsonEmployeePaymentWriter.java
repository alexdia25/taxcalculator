package services.OutputWriters;

import com.fasterxml.jackson.databind.ObjectMapper;
import entities.EmployeePayment;
import exceptions.FatalException;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JsonEmployeePaymentWriter implements EmployeePaymentWriter {
    public void writePayments(List<EmployeePayment> employeePayments, String outputFileName) throws FatalException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File(outputFileName), employeePayments);
        } catch (IOException e) {
            throw new FatalException("Unable to write JSON output to file.", e);
        }
        System.out.println("JSON written to " + outputFileName);
    }
}
