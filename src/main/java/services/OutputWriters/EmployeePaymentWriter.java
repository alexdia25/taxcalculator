package services.OutputWriters;

import entities.EmployeePayment;
import exceptions.FatalException;

import java.util.List;

public interface EmployeePaymentWriter {
    public void writePayments(List<EmployeePayment> employeePayments, String outputFileName) throws FatalException;
}
