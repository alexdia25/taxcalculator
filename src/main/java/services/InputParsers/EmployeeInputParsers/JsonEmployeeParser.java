package services.InputParsers.EmployeeInputParsers;

import com.fasterxml.jackson.databind.ObjectMapper;
import entities.Employee;
import exceptions.FatalException;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class JsonEmployeeParser implements EmployeeParser {

    @Override
    public List<Employee> parseEmployees(InputStream employeeInputFile) throws FatalException {
        ObjectMapper mapper = new ObjectMapper();
        List<Employee> employees = null;
        try {
            employees = mapper.readValue(employeeInputFile, mapper.getTypeFactory().constructCollectionType(List.class, Employee.class));
        } catch (IOException e) {
            throw new FatalException("Unable to read in employee input file. Check data format.", e);
        }
        return employees;
    }
}
