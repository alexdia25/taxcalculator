package services.InputParsers.EmployeeInputParsers;

import entities.Employee;
import exceptions.FatalException;

import java.io.InputStream;
import java.util.List;

public interface EmployeeParser {
    public List<Employee> parseEmployees(InputStream employeeInputFile) throws FatalException;
}
