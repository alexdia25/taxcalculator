package services.InputParsers.EmployeeInputParsers;

import exceptions.FatalException;

public class EmployeeParserFactory {

    public static EmployeeParser getEmployeeParser(String parserType) throws FatalException{
        EmployeeParser parser;
        switch(parserType.toUpperCase()){
            case "JSON":
                parser = new JsonEmployeeParser();
                break;
            default:
                throw new FatalException("EmployeeParser must be called with valid input file type.", new IllegalArgumentException());
        }
        return parser;
    }

}
