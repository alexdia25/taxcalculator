package services.InputParsers.TaxBracketInputParsers;

import entities.TaxBracket;
import exceptions.FatalException;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

public interface TaxBracketParser {
    public HashMap<Integer, List<TaxBracket>> parseTaxBrackets(InputStream taxBracketInputFile) throws FatalException;
}
