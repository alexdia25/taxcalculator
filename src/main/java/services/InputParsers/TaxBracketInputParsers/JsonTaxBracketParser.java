package services.InputParsers.TaxBracketInputParsers;

import com.fasterxml.jackson.databind.ObjectMapper;
import entities.TaxBracket;
import entities.YearlyTaxBracket;
import exceptions.FatalException;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class JsonTaxBracketParser implements TaxBracketParser{
    @Override
    public HashMap<Integer, List<TaxBracket>> parseTaxBrackets(InputStream taxBracketInputFile) throws FatalException {
        ObjectMapper mapper = new ObjectMapper();
        YearlyTaxBracket[] yearlyTaxBrackets = new YearlyTaxBracket[0];
        HashMap taxBracketYearMapping = new HashMap<Integer,List<TaxBracket>>();
        try{
            yearlyTaxBrackets = mapper.readValue(taxBracketInputFile, YearlyTaxBracket[].class);
        } catch (Exception e){
            FatalException fatalException = new FatalException("Unable to read in tax bracket input file. Check data format.", e);
            throw fatalException;
        }
        List<YearlyTaxBracket> taxBrackets = Arrays.asList(yearlyTaxBrackets);
        for (YearlyTaxBracket yearlyTaxBracket : taxBrackets) {
            taxBracketYearMapping.put(yearlyTaxBracket.getStartTaxYear(), yearlyTaxBracket.getTaxBrackets());
        }

        return taxBracketYearMapping;
    }
}
