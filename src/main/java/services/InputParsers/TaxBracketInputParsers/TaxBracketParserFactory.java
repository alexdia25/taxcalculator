package services.InputParsers.TaxBracketInputParsers;

import exceptions.FatalException;

public class TaxBracketParserFactory {

    public static TaxBracketParser getTaxBracketParser(String parserType) throws FatalException{
        JsonTaxBracketParser parser;
        switch(parserType.toUpperCase()){
            case "JSON":
                parser = new JsonTaxBracketParser();
                break;
            default:
                throw new FatalException("TaxBracketParserFactory must be called with valid input file type.", new IllegalArgumentException());
        }
        return parser;
    }

}
