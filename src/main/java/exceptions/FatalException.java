package exceptions;

public class FatalException extends Exception{

    private String customMessage;
    private Exception baseException;

    public String getCustomMessage() {
        return customMessage;
    }

    public void setMessage(String baseMessage) {
        this.customMessage = baseMessage;
    }

    public Exception getBaseException() {
        return baseException;
    }

    public FatalException(String customMessage, Exception baseException){
        this.customMessage = customMessage;
        this.baseException = baseException;
    }

    @Override
    public String getMessage() {
        return this.getCustomMessage() + "\n" + this.getBaseException().getMessage();
    }

    @Override
    public void printStackTrace(){
        System.err.println(this.getCustomMessage());
        this.getBaseException().printStackTrace();
    }

}
