import entities.Employee;
import entities.EmployeePayment;
import entities.TaxBracket;
import exceptions.FatalException;
import javafx.util.Pair;
import org.joda.time.DateTime;
import services.InputParsers.EmployeeInputParsers.EmployeeParser;
import services.InputParsers.EmployeeInputParsers.EmployeeParserFactory;
import services.InputParsers.TaxBracketInputParsers.TaxBracketParser;
import services.InputParsers.TaxBracketInputParsers.TaxBracketParserFactory;
import services.OutputWriters.EmployeePaymentWriter;
import services.OutputWriters.EmployeePaymentWriterFactory;
import services.PaymentCalculators.TaxCalculator;
import services.UserInterfaces.InputFilesStruct;
import services.UserInterfaces.CommandLineUserInterface;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    try {
            InputFilesStruct inputs = CommandLineUserInterface.retrieveInputsFromArguments(args);

            List<Employee> employees = getEmployeeList(inputs.getEmployeeInputStreamType(), inputs.getEmployeeInput());
            HashMap<Integer, List<TaxBracket>> yearTaxBracketsMapping = getYearlyTaxBrackets(inputs.getTaxbracketInputType(), inputs.getTaxBracketInput());

            List<EmployeePayment> employeePaymentList = new ArrayList<>();

            for (Employee employee : employees) {
                Integer taxYear = getTaxYear(employee);
                List<TaxBracket> taxBracketList = yearTaxBracketsMapping.get(taxYear);
                employeePaymentList.add(getEmployeePayment(employee, taxBracketList));
            }

            writePaymentsToFile(employeePaymentList, inputs.getOutputLocation());
        } catch (FatalException e) {
            e.printStackTrace();
        }
    }

    private static List<Employee> getEmployeeList(String employeeInputStreamType, InputStream employeeInputFile) throws FatalException {
        EmployeeParser parser = EmployeeParserFactory.getEmployeeParser(employeeInputStreamType);
        List<Employee> employees = parser.parseEmployees(employeeInputFile);
        return employees;
    }

    private static HashMap<Integer, List<TaxBracket>> getYearlyTaxBrackets(String taxBracketInputStreamType, InputStream taxBracketInputFile) throws FatalException {
        TaxBracketParser parser = TaxBracketParserFactory.getTaxBracketParser(taxBracketInputStreamType);
        HashMap<Integer, List<TaxBracket>> yearTaxBracketsMapping = parser.parseTaxBrackets(taxBracketInputFile);
        return yearTaxBracketsMapping;
    }

    private static Integer getTaxYear(Employee employee) throws FatalException {
        Pair<DateTime,DateTime> paymentDates;
        paymentDates = employee.getPaymentDateRange();
        Integer taxYear = paymentDates.getKey().getYear();
        if(paymentDates.getKey().getMonthOfYear()<7){
            taxYear -= 1;
        }
        return taxYear;
    }

    private static EmployeePayment getEmployeePayment(Employee employee, List<TaxBracket> taxBracketList) {
        TaxCalculator taxCalculator = new TaxCalculator(taxBracketList);
        return new EmployeePayment(employee,taxCalculator);
    }

    private static void writePaymentsToFile(List<EmployeePayment> employeePayments, String outputLocation) throws FatalException {
        EmployeePaymentWriter writer = EmployeePaymentWriterFactory.getEmployeePaymentWriter("JSON");
        writer.writePayments(employeePayments, outputLocation);
    }
}

